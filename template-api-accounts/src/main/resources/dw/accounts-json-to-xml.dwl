%dw 1.0
%output application/xml
---
Account: {
	id: payload.account_id,
	name: payload.account_name,
	type: payload.account_type,
	balance: payload.balance,
	bankName: payload.bank_name,
	currencyCode: payload.currency_code,
	status: payload.is_active,
	uncategorizedTransactions: payload.uncategorized_transactions

}